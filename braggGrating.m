﻿close all;
clear all;

# constants
c = 299792458; 
lambda_d = 1550e-9;
simul_band = 4e-9;
lambda_max = lambda_d + simul_band/2;
lambda_min = lambda_d - simul_band/2;
num  = 1000; #vector lenght
lambda = linspace(lambda_min,lambda_max,num);

FLAG_KL = 0;
neff = 1.447; #silica
L = 3e-3;
grating_name = "Grade B";

if FLAG_KL
    KL = 8;
    kappa = KL/L;
    rmax = tanh(KL)^2;
else
    rmax = 0.8;
    kappa = atanh(rmax^0.5)/L;
    KL = kappa*L;
end

fprintf("KL = %.5f\n",KL);

##parameters
sigma = 0; #no loss absorption
ccoff = 0;

## induced index change ac and dc
dn_ac = kappa*lambda_d/pi #delta_neff
dn_dc = sigma*lambda_d/(2*pi)

##detunning factor
dfdz = (-4*pi*neff*L/lambda_d)*ccoff;
dtt = 2*pi*neff*(1./lambda-1./lambda_d);

## dc self-coupling coefficient
sigma_ = dtt + sigma - dfdz/2;

##power reflection coefficients 
rho=(-kappa.*sinh((kappa^2-sigma_.^2).^0.5*L)) ./ (sigma_.*sinh((kappa^2-sigma_.^2).^0.5*L) + j* (kappa^2-sigma_.^2).^0.5.*cosh((kappa^2-sigma_.^2).^0.5*L));

#r = sinh((kappa^2-sigma_.^2).^0.5*L).^2./(cosh((kappa^2-sigma_.^2).^0.5*L).^2 - sigma_.^2./kappa^2);
r = abs(rho).^2;

gain_dB = 10*log10(abs(r)); ##gain

##band edges
lmbd_max = (1+dn_dc/neff)*lambda_d;
r_band_edges = (kappa*L)^2/(1+(kappa*L)^2);##reflectivity at the band edges
lmbd_band_edge1 = lmbd_max - dn_ac/(2*neff)*lambda_d;
lmbd_band_edge2 = lmbd_max + dn_ac/(2*neff)*lambda_d;
band_edge = dn_ac*lambda_d/neff;
fprintf("Band edge: %.3f nm\n",band_edge*1e9);

##bandwidth
bandwidth = lambda_d^2*(kappa^2*L^2 +pi^2)^0.5/(neff*pi*L);
fprintf("Bandwitdh: %.3f nm\n",bandwidth*1e9);
 
##results

#plot of reflectivity
figure(1);
plot(lambda*1e9,r,"linewidth",2);

grid on;
axis([lambda_min*1e9, lambda_max*1e9, 0, rmax+0.1]);
set(gca,"xtick",lambda_min*1e9:0.5: lambda_max*1e9);
set(gca,'FontSize', 14);

legend({grating_name},"FontSize",14);
xlabel("wavelength(nm)", "FontSize",18);
ylabel("reflectivity","FontSize",18);

hold on
plot([lmbd_band_edge1*1e9,lmbd_band_edge2*1e9],[r_band_edges,r_band_edges],"xb","linewidth",1);
plot([lambda_min*1e9, lambda_max*1e9],[rmax, rmax],"--r","linewidth",2);
plot([lambda_min*1e9, lambda_max*1e9],[r_band_edges, r_band_edges],"--k","linewidth",2);


#plot of gain
figure(2);
plot(lambda*1e9,gain_dB,"linewidth",2);

grid on;
axis([lambda_min*1e9, lambda_max*1e9, -40, 0]);
set(gca,"xtick",lambda_min*1e9:0.5: lambda_max*1e9);
set(gca,"ytick",[0:-5:-40]);
set(gca,'FontSize', 14);

legend({grating_name},"FontSize",14);
xlabel("wavelength(nm)", "FontSize",18);
ylabel("gain(dB)", "FontSize",18);
      
      
      
##rho angle and derivative of rho angle
%{
fase_rho = angle(rho);

dfdl = zeros(1,num);#derivative of rho angle
h = simul_band/(num-1);
dfdl(1) = (fase_rho(2)-fase_rho(1))/h;
dfdl(num) = (fase_rho(num)-fase_rho(num-1))/h;
for i = (2:1:num-1)
    dfdl(i) = (fase_rho(i+1)-fase_rho(i-1))/(2*h);
end

delay_time = -lambda.^2/(2*pi*c).*dfdl;


#plot of delay time
figure(3);
plot(lambda*1e9,delay_time*1e12,"linewidth",2);
grid on;
axis([1544,1556,0,50]);
legend("delay time");
xlabel("wavelength(nm)");
ylabel("delay time(ps)");

%}
